using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D rbody2D;
    [SerializeField] private float upForce = 350f;
    private bool isDead = false;
    private Animator animator;
    public AudioClip SoundCollision;
    public AudioClip SoundGameOver;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        rbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isDead)
        {
            Flap();
        }

        if (isDead == true)
        {
            StopAudio();
        }

    }
    private void StopAudio()
    {
        if (audioSource != null)
        {
            audioSource.Stop();
        }    
    }

    private void Flap()
    {
        animator.SetTrigger("Flap");
        rbody2D.velocity = Vector2.zero;
        rbody2D.AddForce(Vector2.up * upForce);   
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isDead = true;
        animator.SetTrigger("Die");
        GameManager.Instance.GameOver();

        Camera.main.GetComponent<AudioSource>().PlayOneShot(SoundCollision);
        Camera.main.GetComponent<AudioSource>().PlayOneShot(SoundGameOver);
        StopAudio();

    }
}
